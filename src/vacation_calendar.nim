# This is just an example to get you started. A typical binary package
# uses this file as the main entry point of the application.
include karax / prelude

from karax / kajax import ajaxGet

from std / tables import toTable, `[]`, `[]=`, Table, getOrDefault
from std / parsecsv import parsecsv
from streams import newStringStream
from parsecsv import CsvParser, open, readRow, readHeaderRow
from strutils import parseUInt, parseInt
from sequtils import map
from times import now, year

const HOLIDAYS_URL = "https://data.gov.ru/opendata/7708660670-proizvcalendar/data-20191112T1252-structure-20191112T1247.csv?encoding=UTF-8"

type
  Month = enum
    January
    Febrary
    March
    April
    May
    June
    July
    August
    September
    October
    November
    December

  WeekDay = enum
    Monday = 0
    Tuesday
    Wednessday
    Thursday
    Friday
    Saturday
    Sunday

  HolidaysByMonth = Table[Month, seq[int]]

  YearPresenter = ref object of VComponent
    year: uint
    holidays: Table[uint, HolidaysByMonth]

let days_in_month = {
  January: 31,
  Febrary: 28,
  March: 31,
  April: 30,
  May: 31,
  June: 30,
  July: 31,
  August: 31,
  September: 30,
  October: 31,
  November: 30,
  December: 31
}.toTable

proc log() {.importc: "console.log".}

proc renderMonth(holidays: seq[int], first_day: WeekDay, days: range[28..31]): VNode =
  var current_day: int = 1 - first_day.int
  echo holidays
  buildHtml(tdiv):
    table(class="month-content"):
      tr(class="day-head"):
        for name in WeekDay:
          td(class=(if name in [Saturday, Sunday]: "holiday-cell" else: "")):
            h4:
              text(($name).substr(0,2))
      while current_day <= days:
        tr:
          for index in WeekDay:
            let day_number = current_day + index.int
            td(class="day-cell" & (if day_number in holidays: " holiday-cell" else: "")):
              span:
                text(if day_number > 0 and day_number <= days: $day_number else: "")
          current_day += 7

proc renderYear(presenter: YearPresenter): VNode =
  let year = presenter.year
  let holidays = presenter.holidays.getOrDefault(year, HolidaysByMonth())
  # Every year that devides by 4 is a leap year except for those which can be devided by 100
  # But if a year can be devided by 400 it is still a leap year regardless of previous rule
  let is_leap: bool = (year mod 4 == 0) and (year mod 100 != 0 or year mod 400 == 0) 
  # At the first year the year is started from Monday
  # Every new non-leap year the year start shifts by 1 day forward
  # Every leap year the year start shifts by 2 days forward
  # And in the end lets add 6 to adjust the day index to starting from the monday
  let pyear = max(year - 1, 0)
  let first_day_of_year = WeekDay((year + pyear div 400 + pyear div 4 - pyear div 100 + 6) mod 7)
  log($first_day_of_year)
  var last_shift = int(first_day_of_year)
  buildHtml(tdiv):
    tdiv(class="year-head"):
      h2:
        text($year)
    tdiv(class="year-content"):
      for month in Month:
        let days_count = days_in_month[month.Month] + int(is_leap and month == Febrary)
        let first_day_of_month = last_shift.WeekDay
        last_shift = (last_shift + days_count) mod 7
        tdiv(class="month-head"):
          h3:
            text($month)
        renderMonth(holidays.getOrDefault(month, @[]), first_day_of_month, days_count)

proc makeYearPresenter(default_year: uint): YearPresenter =
  result = newComponent(YearPresenter, renderYear)
  result.year = 2022
  proc ondata(status: int, response: cstring) =
    if status != 200:
      return
    let stream = newStringStream($response)
    var parser: CsvParser
    parser.open(stream, "ifile")
    parser.readHeaderRow()
    while parser.readRow():
      let year = parser.row[0].parseUInt
      var year_table: HolidaysByMonth
      result.holidays[year] = year_table
      for month in Month:
        let holidays = parser.row[int(month) + 1].split(",")
        year_table[month] = holidays.map(proc (x: cstring): int = parseInt($x))
    redraw()

  ajaxGet(HOLIDAYS_URL, [(cstring"Origin", cstring"*")], ondata, doRedraw=false)


proc entryPoint() =
  var year_presenter = makeYearPresenter(uint(now().year))
  proc makePage(): VNode =
    buildHtml(tdiv):
      tdiv(class="year-selector"):
        input(class="input", type="number", value=($year_presenter.year)):
          proc onchange(ev: Event, node: VNode) =
            let value = node.value
            log(node.value)
            if not value.isNil:
              let new_year = ($value).parseUInt
              if new_year != year_presenter.year:
                year_presenter.year = new_year
                markDirty(year_presenter)
                redraw()
      year_presenter


  setRenderer(makePage)

entryPoint()
