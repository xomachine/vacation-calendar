# Package

version       = "0.1.0"
author        = "xomachine"
description   = "The vacation planner web app"
license       = "MIT"
srcDir        = "src"
bin           = @["vacation_calendar.js"]
backend       = "js"

# Dependencies

requires "nim >= 1.4.8"
requires "karax"
